@echo off

REM --[ Configuration ]------------------
set "BLENDER_PATH=C:\Program Files\Blender Foundation\Blender 3.4"
REM -------------------------------------

SetLocal EnableDelayedExpansion

REM By default, use GPU 0.
REM Pass parameters -g 1 (or -g=1) to configure another GPU
set GPU=0

:loop
IF NOT "%1"=="" (
    IF "%1"=="-g" (
        SET GPU=%2
        SHIFT
    )
    SHIFT
    GOTO :loop
)

REM Configure the Blender executable to use
set "BLENDER_EXE=blender-gpu%GPU%.exe"
set "FLAMENCO_BLENDER_PATH=%BLENDER_PATH%\%BLENDER_EXE%"

REM By default, use gpu-0, gpu-1 as name
set NAME=gpu-%GPU%

REM Try to improve the GPU name using WMIC.
REM   We use wmic /format:csv to output COMPUTERNAME,NVIDIA GeForce GTX 1080 Ti
REM   We use skip=1 to skip the "Node,Name" header line
REM   We use findstr to skip empty lines
REM   We use string replacement up to and including the comma to get rid of the computer name

REM Video controllers start with 1, so add 1 before lookup
set /A DEVICE_ID=%GPU%+1
for /F "skip=1 delims=" %%A in ('wmic path win32_VideoController where DeviceID^="VideoController%DEVICE_ID%" get name /format:csv ^| findstr /r /v "^$"') do (
	set  NAME=%%A
	set  NAME=!NAME:*,=!
	set  NAME=!NAME:NVIDIA =!
	set  NAME=!NAME: =-!
	set  NAME=!GPU!-!NAME!
)

REM Configure the Flamenco Worker and home directory
set FLAMENCO_HOME=%LOCALAPPDATA%\Blender Foundation\Flamenco\worker-%NAME%
set FLAMENCO_WORKER_NAME=%COMPUTERNAME%-%NAME%
if not exist "%FLAMENCO_HOME%\" (
    echo Creating worker home directory: %FLAMENCO_HOME%
    mkdir "%FLAMENCO_HOME%"
)

REM Run the worker
echo ===================================================================================
echo Running worker: %FLAMENCO_WORKER_NAME%
echo Worker Home:    %FLAMENCO_HOME%
echo Blender path:   %FLAMENCO_BLENDER_PATH%
echo Note: Use Windows or NVidia configuration to assign a GPU to %BLENDER_EXE%!
echo ===================================================================================

flamenco-worker.exe

REM Clean up all variables
set BLENDER_EXE=
set BLENDER_PATH=
set DEVICE_ID=
set FLAMENCO_BLENDER_PATH=
set FLAMENCO_HOME=
set FLAMENCO_WORKER_NAME=
set GPU=
set NAME=

EndLocal
