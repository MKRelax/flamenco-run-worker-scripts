@echo off

IF "%1"=="" goto Usage

REM Read the worker name from the first parameter
set "WORKER_NAME=%1"

REM Configure the Flamenco Worker and home directory
set FLAMENCO_HOME=%LOCALAPPDATA%\Blender Foundation\Flamenco\worker-%WORKER_NAME%
set FLAMENCO_WORKER_NAME=%COMPUTERNAME%-%WORKER_NAME%

REM Create a configuration folder if it does not exist
if not exist "%FLAMENCO_HOME%\" (
    echo Creating worker home directory: %FLAMENCO_HOME%
    mkdir "%FLAMENCO_HOME%"
)

REM Run the worker
echo ===================================================================================
echo Running worker: %FLAMENCO_WORKER_NAME%
echo Worker Home:    %FLAMENCO_HOME%
echo ===================================================================================

flamenco-worker.exe

REM Clean up environment variables
set FLAMENCO_HOME=
set FLAMENCO_WORKER_NAME=
set WORKER_NAME=

goto End

:Usage

echo Please provide a name for this worker.
pause

:End